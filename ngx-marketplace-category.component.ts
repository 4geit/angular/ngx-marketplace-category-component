import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-marketplace-category',
  template: require('pug-loader!./ngx-marketplace-category.component.pug')(),
  styleUrls: ['./ngx-marketplace-category.component.scss']
})
export class NgxMarketplaceCategoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
