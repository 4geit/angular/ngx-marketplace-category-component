import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceCategoryComponent } from './ngx-marketplace-category.component';

describe('marketplace-category', () => {
  let component: marketplace-category;
  let fixture: ComponentFixture<marketplace-category>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-category ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-category);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
