# @4geit/ngx-marketplace-category-component [![npm version](//badge.fury.io/js/@4geit%2Fngx-marketplace-category-component.svg)](//badge.fury.io/js/@4geit%2Fngx-marketplace-category-component)

---

category section for marketplace

## Installation

1. A recommended way to install ***@4geit/ngx-marketplace-category-component*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-marketplace-category-component) package manager using the following command:

```bash
npm i @4geit/ngx-marketplace-category-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-marketplace-category-component
```

2. You need to import the `NgxMarketplaceCategoryComponent` component within the module you want it to be. For instance `app.module.ts` as follows:

```js
import { NgxMarketplaceCategoryComponent } from '@4geit/ngx-marketplace-category-component';
```

And you also need to add the `NgxMarketplaceCategoryComponent` component with the `@NgModule` decorator as part of the `declarations` list.

```js
@NgModule({
  // ...
  declarations: [
    // ...
    NgxMarketplaceCategoryComponent,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. You can also attach the component to a route in your routing setup. To do so, you need to import the `NgxMarketplaceCategoryComponent` component in the routing file you want to use it. For instance `app-routing.module.ts` as follows:

```js
import { NgxMarketplaceCategoryComponent } from '@4geit/ngx-marketplace-category-component';
```

And you also need to add the `NgxMarketplaceCategoryComponent` component within the list of `routes` as follows:

```js
const routes: Routes = [
  // ...
  { path: '**', component: NgxMarketplaceCategoryComponent }
  // ...
];
```
