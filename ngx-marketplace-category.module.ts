import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMarketplaceCategoryComponent } from './ngx-marketplace-category.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxMarketplaceCatalogModule } from '@4geit/ngx-marketplace-catalog-component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
    NgxMarketplaceCatalogModule,
  ],
  declarations: [
    NgxMarketplaceCategoryComponent
  ],
  exports: [
    NgxMarketplaceCategoryComponent
  ]
})
export class NgxMarketplaceCategoryModule { }
